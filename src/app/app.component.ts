import { Component, OnInit } from '@angular/core';
import { CartService } from './shared/services/cart.service';
import { MediaService } from './shared/services/media.service';
import { SearchService } from './shared/services/search.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
	title = 'shop-app';
	isMediumScreen: boolean = this.mediaService.screenInfo$.getValue().isMediumScreen;
	isCartOpen = false;

	constructor(
		public cartService: CartService,
		public mediaService: MediaService,
		private searchService: SearchService
	) { }

	ngOnInit(): void {
		this.mediaService.screenInfo$.subscribe(screen => this.isMediumScreen = screen.isMediumScreen);
		this.mediaService.cartSideNav$.subscribe(isCartNavOpen => this.isCartOpen = isCartNavOpen);
	}

	/**
	 * Triggers when user is typing in the search input field.
	 * 
	 * @param name the name of the product typed by user
	 */
	onSearch(name: string) {
		this.searchService.patchSearchCondition({ name });
	}
}
