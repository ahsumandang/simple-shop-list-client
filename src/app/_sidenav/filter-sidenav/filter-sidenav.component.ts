import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { SearchService } from '@shared/services';

@Component({
	selector: 'app-filter-sidenav',
	templateUrl: './filter-sidenav.component.html',
	styleUrls: ['./filter-sidenav.component.sass']
})
export class FilterSidenavComponent implements OnInit, OnDestroy {
	public filterForm: FormGroup = this.formBuilder.group({
		minPrice: ['', Validators.min(0)],
		maxPrice: ['', Validators.min(0)]
	});

	private toDestroy$ = new Subject();

	constructor(
		private formBuilder: FormBuilder,
		private searchService: SearchService
	) { }

	ngOnInit(): void {
		this.filterForm.valueChanges.pipe(takeUntil(this.toDestroy$)).subscribe(filter =>
			this.searchService.patchSearchCondition(filter));
	}

	ngOnDestroy() {
		this.toDestroy$.next();
		this.toDestroy$.complete();
	}
}
