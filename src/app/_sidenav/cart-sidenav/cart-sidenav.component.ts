import { Component } from '@angular/core';

import { CartService } from '@shared/services';

@Component({
	selector: 'app-cart-sidenav',
	templateUrl: './cart-sidenav.component.html',
	styleUrls: ['./cart-sidenav.component.sass']
})
export class CartSidenavComponent {

	constructor(public cartService: CartService) { }

	/**
	 * Gets the total price of the items in the cart.
	 */
	get totalPrice(): number {
		return this.cartService.addedProducts
			.reduce((total, product) => total + product.product.price * product.qty, 0)
	}
}
