import { SearchCondition } from "@shared/services/search.service";

export class Product {
	id = '';
	name = '';
	price = 0;
	description = '';
	photo = '';
	constructor(product?: any) {
		if (product) {
			this.id = product._id;
			this.name = product.name;
			this.price = product.price;
			this.description = product.description;
			this.photo = product.photo;
		}
	}

	/**
	 * Checks if this product matches the search condition.
	 *
	 * @param searchCondition properties to check if values are matching with product
	 * @returns true if all conditions are met; false if otherwise.
	 */
	isMatch(searchCondition: SearchCondition) {
		const searchName = searchCondition.name?.toLocaleLowerCase() ?? '';
		const isNameMatching = this.name.toLocaleLowerCase().indexOf(searchName) >= 0;
		const isWithinPriceRange = (!searchCondition.minPrice || searchCondition.minPrice <= this.price)
			&& (!searchCondition.maxPrice || this.price <= searchCondition.maxPrice);
		return isNameMatching && isWithinPriceRange;
	}
}