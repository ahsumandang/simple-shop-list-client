import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface SearchCondition {
	name?: string;
	minPrice?: number,
	maxPrice?: number
}

@Injectable({
	providedIn: 'root'
})
export class SearchService {
	public searchCondition$: BehaviorSubject<SearchCondition> = new BehaviorSubject({});

	constructor() { }

	get hasSearchFilter$(): Observable<boolean> {
		return this.searchCondition$.pipe(
			map(cond => !!cond.name || !!cond.minPrice || !!cond.maxPrice)
		)
	}

	/**
	 * Patches search condition values.
	 * Emits new search condition to subscriptions.
	 *
	 * @param filter the properties to be patched to existing search condition.
	 */
	patchSearchCondition(filter: SearchCondition) {
		const newFilter = {
			...this.searchCondition$.value,
			...filter
		};

		this.searchCondition$.next(newFilter);
	}
}
