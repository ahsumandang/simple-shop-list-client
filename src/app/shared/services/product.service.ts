import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'
import { Product } from '../models/product';

@Injectable({
	providedIn: 'root'
})
export class ProductService {

	constructor(private http: HttpClient) { }

	/**
	 * Gets a product.
	 * 
	 * @param productID the ID of the product to be retrieved.
	 * @returns an Observable that the retrieved product
	 */
	getProduct(productID: string): Observable<Product> {
		return this.http.get<any>(`/api/products/${productID}`).pipe(
			map(product => new Product(product))
		);
	}

	/**
	 * Gets all product from the database.
	 * 
	 * @returns an Observable that emits all the products retrieved from the database.
	 */
	getAllProducts(): Observable<Product[]> {
		return this.http.get<any[]>('/api/products/all').pipe(
			map(products => products.map(product => new Product(product)))
		);
	}
}
