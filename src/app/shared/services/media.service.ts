import { Injectable, OnDestroy } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { BehaviorSubject, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

export interface ScreenInfo {
	isSmallScreen: boolean; // max-width of 959.98px
	isMediumScreen: boolean; // min-width: 960px,
}

@Injectable({
	providedIn: 'root'
})
export class MediaService implements OnDestroy {
	/**
	 * Emits screen info either when resized or sidenav is closed/opened.
	 *
	 * @returns {BehaviorSubject<ScreenInfo>} Screen info such as isSmallScreen, isCartSideNavOpen, etc.
	 * @see {@link ScreenInfo}
	 */
	public screenInfo$: BehaviorSubject<ScreenInfo> = new BehaviorSubject<ScreenInfo>(this.screenInfo);

	/**
	 * Emits value if the cart is to be opened.
	 *
	 * @returns {Subject<boolean>} Value when the cart is to be opened.
	 * @see {@link ScreenInfo}
	 */
	public cartSideNav$: Subject<boolean> = new Subject<boolean>();
	private toDestroy$ = new Subject();

	constructor(private breakpointObserver: BreakpointObserver) {
		this.setScreenInfoSubject();
	}

	ngOnDestroy() {
		this.toDestroy$.next();
		this.toDestroy$.complete();
	}

	/**
	 * Gets the screen info (current screen size, etc).
	 * 
	 * @returns the screen info like current screen size state (isSmallScreen, isMediumScreen).
	 */
	private get screenInfo(): ScreenInfo {
		const isSmallScreen = this.breakpointObserver.isMatched([Breakpoints.XSmall, Breakpoints.Small]);
		return {
			isSmallScreen,
			isMediumScreen: isSmallScreen || this.breakpointObserver.isMatched(Breakpoints.Medium)
		};
	}

	/**
	 * Set side navigation subject/observable when the screen is resized.
	 */
	private setScreenInfoSubject() {
		// Side Navigation (toggling on resize)
		this.breakpointObserver.observe([Breakpoints.XSmall, Breakpoints.Small, Breakpoints.Medium])
			.pipe(
				takeUntil(this.toDestroy$),
				map(() => this.screenInfo)
			).subscribe(result => this.screenInfo$.next(result));
	}
}
