import { Injectable } from '@angular/core';
import { Product } from '../models/product';
import { MediaService } from './media.service';

@Injectable({
	providedIn: 'root'
})
export class CartService {
	public addedProducts: { product: Product; qty: number }[] = [];

	constructor(private mediaService: MediaService) { }

	/**
	 * Adds a product to the cart.
	 *
	 * @param product the product to add to cart
	 * @param qty (optional) quantity of the product to be added
	 */
	addProduct(product: Product, qty: number = 1) {
		const index = this.addedProducts.findIndex(prod => prod.product.id === product.id);
		if (index >= 0) {
			this.addedProducts[index].qty += qty;
		} else {
			this.addedProducts.push({ product, qty });
		}

		console.log('add');
		this.mediaService.cartSideNav$.next(true);
	}

	/**
	 * Remove/deduct quantity of a product in the cart.
	 * @param productID ID of the product to be removed or deducted
	 * @param qty (optional) quantity to deduct; if not supplied, assumes to remove the product entirely
	 * from the cart
	 */
	removeProduct(productID: string, qty?: number) {
		const index = this.addedProducts.findIndex(prod => prod.product.id === productID);
		if (index >= 0) {
			if (qty && this.addedProducts[index].qty - qty !== 0) {
				this.addedProducts[index].qty -= qty;
			} else { // 
				this.addedProducts.splice(index, 1);
			}
		}
	}
}
