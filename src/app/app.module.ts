import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from '@shared/modules/material/material.module';
import { AppComponent } from './app.component';
import { ProductItemComponent } from './products/product-item/product-item.component';
import { ProductListComponent } from './products/product-list/product-list.component';
import { ProductDialogComponent } from './products/product-dialog/product-dialog.component';
import { CartSidenavComponent } from './_sidenav/cart-sidenav/cart-sidenav.component';
import { FilterSidenavComponent } from './_sidenav/filter-sidenav/filter-sidenav.component';

@NgModule({
	declarations: [
		AppComponent,
		CartSidenavComponent,
		FilterSidenavComponent,
		ProductItemComponent,
		ProductListComponent,
		ProductDialogComponent,
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		FormsModule,
		HttpClientModule,
		MaterialModule,
		ReactiveFormsModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
