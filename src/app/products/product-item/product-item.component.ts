import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Product } from 'src/app/shared/models/product';
import { CartService } from 'src/app/shared/services/cart.service';
import { ProductDialogComponent } from '../product-dialog/product-dialog.component';

@Component({
	selector: 'app-product-item',
	templateUrl: './product-item.component.html',
	styleUrls: ['./product-item.component.sass']
})
export class ProductItemComponent {
	@Input() product!: Product;

	constructor(
		public cartService: CartService,
		private dialog: MatDialog
	) { }

	/**
	 * Opens a product dialog on 'View' button click.
	 * @param product the product to be display in dialog
	 */
	onOpenProductDialog(product: Product) {
		this.dialog.open(ProductDialogComponent, {
			panelClass: 'full-screen-dialog', width: 'min(600px, 100vw)', maxWidth: '100vw', data: product
		})
	}
}