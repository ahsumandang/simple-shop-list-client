import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Product } from '@shared/models';
import { ProductService, SearchCondition, SearchService } from '@shared/services';

@Component({
	selector: 'app-product-list',
	templateUrl: './product-list.component.html',
	styleUrls: ['./product-list.component.sass']
})
export class ProductListComponent implements OnInit, OnDestroy {
	products: Product[] = [];
	toDestroy$ = new Subject();
	searchCondition: SearchCondition = {};

	constructor(
		public productService: ProductService,
		public searchService: SearchService
	) { }

	ngOnInit(): void {
		this.productService.getAllProducts()
			.pipe(takeUntil(this.toDestroy$))
			.subscribe(products => this.products = products);

		this.searchService.searchCondition$
			.pipe(takeUntil(this.toDestroy$))
			.subscribe(cond => this.searchCondition = cond);
	}

	/**
	 * Gets the filtered product list based on search condition.
	 */
	get filteredProducts(): Product[] {
		return this.products.filter(prod => prod.isMatch(this.searchCondition));
	}

	ngOnDestroy() {
		this.toDestroy$.next();
		this.toDestroy$.complete();
	}
}
