import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Product } from '@shared/models';
import { CartService } from '@shared/services';

@Component({
	selector: 'app-product-dialog',
	templateUrl: './product-dialog.component.html',
	styleUrls: ['./product-dialog.component.sass']
})
export class ProductDialogComponent {
	isShowMore = false;
	quantity = 1;

	constructor(
		private cartService: CartService,
		@Inject(MAT_DIALOG_DATA) public selectedProduct: Product
	) { }

	/**
	 * Adds current selected product to cart.
	 */
	onAddToCart() {
		this.cartService.addProduct(this.selectedProduct, this.quantity);
	}
}
