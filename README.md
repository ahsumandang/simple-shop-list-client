# ShopApp Client

The client-side application of an app where you can view the list of products and add them to your cart.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.2.

## How to Set Up

### Prerequisite
Make sure the following technologies are set up in your local. You can use higher version but this is my local version.
- Angular (v12.2.2)
- Node (v14.15.4)
- NPM (6.14.10)

### Execution Steps

Important Note: the following steps must be followed sequentially.
1. In the command line of your choice, go to `/shop-app` directory and type in `npm install` (requires internet). Make sure there are no error messages.
2. Enter `npm start` next in the command line. "Compiled successfully." will be displayed if running correctly.
4. Go to [localhost:4200](http://localhost:4200).

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running linter

Run `ng lint`, `ng l` or  or `ng l --fix` to execute the linter. see `https://github.com/angular-eslint/angular-eslint` for more information.